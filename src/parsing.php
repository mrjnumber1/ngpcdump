<?php

	function GetCharFrameData(int $addr)
	{
		global $rom_bytes;
		$addr = $start = intval(AddrRam2Rom($addr));

		$bytes = [];
		
		for ($i=0; $i < 200; ++$i)
			$bytes[$i] = bin2hex($rom_bytes[$addr+$i]);
		
		foreach ($bytes as &$b)
			$b = intval($b, 16);
			
		$tiles = $bytes[1]<<16|$bytes[0];
		assert($tiles < 50);
		
		$length = 2;
		$read_tiles = 0;
		$i=2;
		while ($read_tiles < $tiles)
		{
			$value = $bytes[$i+1]<<8|$bytes[$i];
			if ($value == 0xFFFF)
			{
				$read_tiles += $bytes[$i+3]<<8|$bytes[$i+2];
				$i += 8;
				continue;
			}
			$read_tiles += 1;
			$i += 4;
		}
		$length = $i;
		
		return array_slice($bytes, 0, $length);
	}
	
	function DecodeCharFrame(array $values)
	{
		assert(is_array($values));
		
		foreach ($values as &$v)
			$v = intval($v, 16);
		
		$count = $values[1]<<16|$values[0];
		
		$tiles = [];
		$xpos = [];
		$ypos = [];
		for ($i=2; $i < count($values); $i+= 4)
		{
			$tile = $values[$i+1]<<8|$values[$i];
			if ($tile == 0xFFFF)
			{
				// we have some repeating, so!
				$continues = $values[$i+3]<<8|$values[$i+2];
				$first_tile = $values[$i+5]<<8|$values[$i+4];
				$first_x = $values[$i+6];
				if ($first_x >= 128)
					$first_x -= 256;
				$first_y = $values[$i+7];
				for ($j = 0; $j < $continues; ++$j)
				{
					$tiles[] = CHARFX_START + (($first_tile+$j)<<4);
					$xpos[] = $first_x;
					$y = $first_y+$j*TILE_HEIGHT;
					if ($y >= 128)
						$y -= 256;
					$ypos[] = $y;
				}
				
				$i+= 4;
				continue;
			}
			
			$tiles[] = CHARFX_START + ($tile<<4);
			$x = $values[$i+2];
			$y = $values[$i+3];
			if ($x >= 128) $x -= 256;
			if ($y >= 128) $y -= 256;
			$xpos[] = $x;
			$ypos[] = $y;
			
		}
		$decoded['tiles'] = $tiles;
		$decoded['x'] = $xpos;
		$decoded['y'] = $ypos;

		return $decoded;
	}
	
	function DecodeTile(int $offset, $flipped = false, $mirrored = false)
	{
		global $rom_bytes;
		
		$offset = AddrRam2Rom($offset);
		if (GAME === 'svc' || GAME === 'kofr1')
			assert(($offset&0x0F) == 0, 'unaligned offset specified in DecodeTile!');
		assert($offset < ROM_LENGTH, 'tile offset requested is past EOF!');
		
		$tile_data = [];
		for ($i=0; $i < BYTES_PER_TILE; ++$i)
			$tile_data[$i] = bin2hex($rom_bytes[$offset+$i]);
		
		$pixels = [];
		
		for ($i=0; $i < BYTES_PER_TILE; $i += 2)
		{
			$data[0] = intval($tile_data[$i+1],16)>>4;
			$data[1] = intval($tile_data[$i+1],16)&0xF;
			$data[2] = intval($tile_data[$i],16)>>4;
			$data[3] = intval($tile_data[$i],16)&0x0F;
			foreach ($data as $nibble)
			{
				$pixels[] = $nibble>>2;
				$pixels[] = $nibble&0x3;
			}
			
		}
		
		if ($flipped)
		{
			// start from the top row and fill the pixels in across
			// px 0, 1, 2.. --> px 56, 57, 58..
			//  8, 9, 10, --> 48, 49
			// 16, 17,  --> 40, 41
			// 24, 25, --> 32 ..
			// 32, --> 24
			$flipped = [];
			$row = 0;
			$col = 0;

			for ($i=0; $i < TILE_HEIGHT*TILE_WIDTH; ++$i)
			{	
				if ($i && $i%8==0)
				{
					$row++;
					$col = 0;
				}
				$offset = (TILE_HEIGHT*TILE_WIDTH)-(($row+1)*TILE_HEIGHT)+$col++;			
				$flipped[$i] = $pixels[$offset];
			}
			$pixels = $flipped;
	
		}
		if ($mirrored)
		{
			// start from top row and fill the pixels in backwards
			$mirrored = [];
			$row=$col=0;
			for ($i=0; $i < TILE_HEIGHT*TILE_WIDTH; ++$i)
			{	
				if ($i && $i%8==0)
				{
					$row++;
					$col = 0;
				}
				$offset = ($row+1)*TILE_WIDTH-$i%8-1;
				$mirrored[$i] = $pixels[$offset];
			}
			$pixels = $mirrored;
		}
		
		return $pixels;
	}

	function GetGalAnimData(int $anim_addr, int $diff = 515)
	{
		global $rom_bytes;
		
		$anim_addr = intval(AddrRam2Rom($anim_addr));
		$bytes = [];
		
		for ($i=0; $i < $diff; ++$i)
			$bytes[$i] = bin2hex($rom_bytes[$anim_addr+$i]);
		
		foreach ($bytes as &$b)
			$b = intval($b, 16);
		
		unset($b);
		
		$length = 0;
		//if ($diff != 515) $length = $diff; // we assume this is fine, but must check anyways.
		
		for ($i=8; $i < $diff && !$length; $i+=8)
			if ($bytes[$i] == 0xFF || $bytes[$i] == 0xFE)
				$length = $i;

		$data = array_slice($bytes, 0, $length);
		
		unset($bytes);
		//$details['base'] = dechex($anim_addr);
		for ($i = 0; $i < $length-1; $i+= 8)
		{
			$details[$i/8]['time'] = $data[$i];
			$details[$i/8]['frame'] = $data[$i+1];
			$details[$i/8]['off1'] = ($data[$i+2] > 128 ? $data[$i+2]-256:$data[$i+2]);
			$details[$i/8]['off2'] = ($data[$i+3] > 128 ? $data[$i+3]-256:$data[$i+3]);
			$details[$i/8]['unknown4'] = $data[$i+4];
			$details[$i/8]['unknown5'] = $data[$i+5];
			$details[$i/8]['unknown6'] = $data[$i+6];
			$details[$i/8]['unknown7'] = $data[$i+7];
			$time = $data[$i];
			$frame = $data[$i+1]<<3;
			$dunno1 = $data[$i+2]<<1;
			$dunno2 = $data[$i+3]<<1;
		}
		
		//$details['repeats'] = ($data[$length-1]);
		
		
		return $details;
	}
	
	function GetGalFrameData(int $frame_addr, bool $is_fx)
	{
		global $rom_bytes;
		
		$frame_addr = intval(AddrRam2Rom($frame_addr));
		$bytes = [];
		
		for ($i=0; $i < 2000; ++$i)
			$bytes[$i] = bin2hex($rom_bytes[$frame_addr+$i]);
		
		foreach ($bytes as &$b)
			$b = intval($b, 16);

		$length = 0;
		if ($is_fx)
			$length = 2+2*$bytes[0]*$bytes[1];
		else
			$length = 1+4*$bytes[0];
		
		assert($length < 2000, 'a way too long frame ('.$length.' bytes) was found at '.dechex($frame_addr).'.. aw fiddle');

		$slice = array_slice($bytes, 0, $length);
		
		return $slice;
	}
	
	function DecodeGalFrame(array $values, int $graphics_addr, bool $is_fx)
	{
		$data = null;
		
		if (!$is_fx)
		{
			$max_id = $values[0];
			
			for ($i=1; $i < count($values); $i += 4)
			{
				$num = $values[$i]-1;
				$tile_display = $values[$i+1];
				$tile_length = ($tile_display&0x10);
				assert($tile_length == 16);
				
				$x = $values[$i+2];
				$y = $values[$i+3];
				
				$data['tiles'][] = $graphics_addr+($num*$tile_length);
				$data['mirror'][] = (($tile_display&(TILE_MIRROR_BIT))>0);
				$data['flip'][] = (($tile_display&(TILE_FLIP_BIT))>0);
				$data['x'][] = $x;
				$data['y'][] = $y;
			}
			
		}
		else
		{
			$y_start = $x_start = -8;
			$height = $values[0];
			$width = $values[1];
			
			$end = $height*$width;
			$row = $col = 0;
			for ($i= 2; $i < count($values); $i+=2)
			{
				
				$tile_display = $values[$i+1]<<8|$values[$i];
				$tile = ($tile_display&0x3F);

				if ($values[$i]&0x80)
				{
					$tile = 0x40+$tile;
				}
				if ($tile <= 0)
					$tile = null;
				else
					$tile = $graphics_addr+($tile-1)*BYTES_PER_TILE;
				
				
				$x = $x_start + TILE_WIDTH*$col;
				$y = $y_start + TILE_HEIGHT*$row;
				if (++$col >= $width)
				{
					assert($row < $height);
					$row++;
					$col = 0;
				}
				
				$data['tiles'][] = $tile;
				$data['flip'][] = ($tile_display&0x4000)>0;
				$data['mirror'][] = ($tile_display&0x8000)>0;//(($tile_display&TILE_MIRROR_BIT<<1)>0);
				$data['x'][] = $x;
				$data['y'][] = $y;
			}
			
		}
		return $data;
	}
	
	function GetFxFrameData(int $addr)
	{
		global $rom_bytes;

		$addr = $start = intval(AddrRam2Rom($addr));

		$bytes = [];
		
		for ($i=0; $i < 800; ++$i)
			$bytes[$i] = bin2hex($rom_bytes[$addr+$i]);
		
		foreach ($bytes as &$b)
			$b = intval($b, 16);

		$width = $bytes[2];
		$height = $bytes[3];
		$length = 0;
		switch (GAME)
		{
		case 'svc':
			$tile_count = $bytes[7]<<8|$bytes[6];
			$length = 8 + $width*$height+$tile_count*2;
			break;
		case 'kofr2':
		case 'kofr1':
			$length = 4 + $width*$height*2;
			break;
		}
		
		
		assert($length < 800, 'a way too long frame ('.$length.' bytes) was found at '.dechex($addr).'.. aw fiddle');
		
		return array_slice($bytes, 0, $length);
	}
	
	function DecodeFxFrame(array $values)
	{
		
		foreach ($values as &$v)
			$v = intval($v, 16);
			
		$x_start = $values[0];
		$y_start = $values[1];
		$frame_width = $values[2];
		$frame_height = $values[3];
		$tiles_start = $tile_count = 0;
		
		$first = 0;
		
		$arrangement_length = $frame_width*$frame_height;
		
		switch (GAME)
		{
		case 'kofr2':
		case 'kofr1':
			$first = 4;
			$arrangement_length *= 2;
			$arrangement_length += $first;
			break;
		case 'svc':
			$first = 8;
			$tiles_start = $values[5]<<8|$values[4];
			$tile_count = $values[7]<<8|$values[6];
			break;
		}
				
		$tiles = $xpos = $ypos = $flip = $mirror = $repeat = [];
		$row=$col=0;

		for ($i=0; $i < $arrangement_length; ++$i)
		{
			$tile = null;
			$mirrored = $flipped = false;
			
			switch (GAME)
			{
			case 'svc':
				$tile_display = $values[8+$i];
				$gfxoffset = 2*($tile_display&0x3F)+$tiles_start;
				if ($tile_display == 0xFF)
				{
					$tile = null;
					$mirrored = false;
					$flipped = false;
				}
				else
				{
					$tile = FX_START+(($values[$gfxoffset+1]<<8|$values[$gfxoffset])<<4);
					$mirrored = ($tile_display&TILE_MIRROR_BIT)>0;
					$flipped = ($tile_display&TILE_FLIP_BIT)>0;
				}
				break;
			case 'kofr2':
			case 'kofr1':
				if (!$i) $i=$first;
				$tile_display = $values[$i+1]<<8 | $values[$i];
				$tile = $tile_display&0x3FFF;
				
				$repeats = ($tile&0x2000)>0;
				if ($tile)
					$tile = FX_START+($tile<<4);
				
				if ($repeats > 0) // repeat the nth tile given 
				{
					$num = $tile_display&0x1FFF;
					// except that repeats don't count in the 0..n range
					$j=0;
					for ($j = 0; $j < count($tiles); ++$j)
					{
						if ($repeat[$j] || $tiles[$j] == 0x00)
							continue;
						
						if ($num-- <= 0)
							break;
					}
					
					$tile = $tiles[$j];
				}
				//if ($tile_display==0x2010)
				//	$tile = 30;
				$mirrored = ($tile_display&(TILE_MIRROR_BIT<<8))>0;
				$flipped = ($tile_display&(TILE_FLIP_BIT<<8))>0; // sorry for the bits not matching. weird.
				if (!$repeats && $tile == 0)
					$tile = null;
				$repeat[] = $repeats;
				$i++; // advance $i again in this case
				break;
			}
			
			
			if (is_null($tile))
				$tiles[] = null;
			else
				$tiles[] = $tile;
			
			$mirror[] = $mirrored;
			$flip[] = $flipped;
			
			$x = $x_start + TILE_WIDTH*$col;
			$y = $y_start + TILE_HEIGHT*$row;
			if ($x > 128) $x -= 256;
			if ($y > 128) $y -= 256;
			$xpos[] = $x;
			$ypos[] = $y;
			
			if (++$col >= $frame_width)
			{
				$row++;
				$col = 0;
			}
		}
		
		$result['tiles'] = $tiles;
		$result['flip'] = $flip;
		$result['mirror'] = $mirror;
		$result['x'] = $xpos;
		$result['y'] = $ypos;
		
		return $result;
	}
	