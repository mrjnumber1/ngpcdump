<?php

	define('DIR_ROMS', 'roms');
	define('DIR_OUTPUT', 'output');
	
	define('HIROM_BEGIN', 0x200000);
	define('RAMROM_START', 0x200000);
	define('RAMHIROM_START', 0x800000);
	define('BITS_PER_PIXEL', 2);
	define('BYTES_PER_TILE', 16);
	define('TILE_WIDTH', 8);
	define('TILE_HEIGHT', 8);
	define('TILE_FLIP_BIT', 0x40);
	define('TILE_MIRROR_BIT', 0x80);

	$CONST = [ 
		'svc' => [
			'length' => 4*1024*1024, 'charfx_start' => AddrRom2Ram(0x200000), 'fx_start' => AddrRom2Ram(0x359AE0), 'first_fx' => 0x9A40CC, 'last_fx' => 0x9B2860, 'first_char' => 0x9B286C, 'last_char' => 0x9E8CF0, 'char_width' => 1500, 'char_height' => 17210, 'fx_width' => 2200, 'fx_height' => 11600,
		],
		'kofr1' => [
			'length' => 2*1024*1024, 'charfx_start' => AddrRom2Ram(0x128B90), 'fx_start' => AddrRom2Ram(0x114160), 'first_fx' => 0x2CB3CC, 'last_fx' => 0x2D1B75, 'first_char' => 0x2D1B7A, 'last_char' => 0x2F53C0, 'char_width' => 1500, 'char_height' => 11310, 'fx_width' => 2100, 'fx_height' => 6750,
		],
		'kofr2' => [
			'length' => 2*1024*1024, 'charfx_start' => AddrRom2Ram(0x70946), 'fx_start' => AddrRom2Ram(0x2D56E), 'first_fx' => 0x244C62, 'last_fx' => 0x24AE78, 'first_char' => 0x24EEA4, 'last_char' => 0x27094C, 'char_width' => 1550, 'char_height' => 10900, 'fx_width' => 2100, 'fx_height' => 7250,
		],
		'snkgalsj' => [ 
			'length' => 2*1024*1024, 'charfx_start' => 0,'fx_start' => 0,'first_fx' => 0, 'last_fx' => 0, 'first_char' => 0x2BDB6A, 'last_char' => 0x2BDB6A+0x30, 'char_width' => 1550, 'char_height' => 29100, 'fx_width' => 0, 'fx_height' => 0,
		],
		'snkgals' => [ 
			'length' => 2*1024*1024, 'charfx_start' => 0, 'fx_start' => 0, 'first_fx' => 0, 'last_fx' => 0, 'first_char' => 0x2BDE5C, 'last_char' => 0x2BDE5C+0x30, 'char_width' => 1550, 'char_height' => 2000, 'char_height' => 29100, 'fx_width' => 0, 'fx_height' => 0,
		],
		'lastblad' => [
			'length' => 2*1024*1024, 'charfx_start' => 0, 'fx_start' => 0, 'first_fx' => 0, 'last_fx' => 0, 'first_char' => 0, 'last_char' => 0, 'char_width' => 0, 'char_height' => 0, 'char_height' => 0, 'fx_width' => 0, 'fx_height' => 0,
		],
		'samsho' => [
			'length' => 2*1024*1024, 'charfx_start' => 0, 'fx_start' => 0, 'first_fx' => 0, 'last_fx' => 0, 'first_char' => 0, 'last_char' => 0, 'char_width' => 0, 'char_height' => 0, 'char_height' => 0, 'fx_width' => 0, 'fx_height' => 0,
		],
		'samsho2' => [
			'length' => 2*1024*1024, 'charfx_start' => 0, 'fx_start' => 0, 'first_fx' => 0, 'last_fx' => 0, 'first_char' => 0, 'last_char' => 0, 'char_width' => 0, 'char_height' => 0, 'char_height' => 0, 'fx_width' => 0, 'fx_height' => 0,
		],
		'fatfury' => [
			'length' => 2*1024*1024, 'charfx_start' => 0, 'fx_start' => 0, 'first_fx' => 0, 'last_fx' => 0, 'first_char' => 0, 'last_char' => 0, 'char_width' => 0, 'char_height' => 0, 'char_height' => 0, 'fx_width' => 0, 'fx_height' => 0,
		],
	];
	
	
	function DefineROMGlobals(string $filename, array $const)
	{
		
		define('GAME', $filename);
		
		define('ROM_LENGTH', $const[GAME]['length']);
		define('CHARFX_START', $const[GAME]['charfx_start']);
		define('FX_START', $const[GAME]['fx_start']);
		define('FIRST_CHAR',$const[GAME]['first_char']);
		define('LAST_CHAR', $const[GAME]['last_char']);
		define('FIRSTFX', $const[GAME]['first_fx']);
		define('LASTFX', $const[GAME]['last_fx']);
		define('FXIMG_HEIGHT', $const[GAME]['fx_height']);
		define('FXIMG_WIDTH', $const[GAME]['fx_width']);
		define('CHARIMG_HEIGHT', $const[GAME]['char_height']);
		define('CHARIMG_WIDTH', $const[GAME]['char_width']);
	}