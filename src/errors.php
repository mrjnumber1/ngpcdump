<?php 

	function handleError($code, $description, $file = null, $line = null, $context = null) {
		$displayErrors = ini_get("display_errors");
		$displayErrors = strtolower($displayErrors);
		if (error_reporting() === 0 || $displayErrors === "on") {
			return false;
		}
		list($error, $log) = mapErrorCode($code);
		if (array_key_exists('rom_bytes', $context))
			$context['rom_bytes'] = null;
		$data = array(
			'level' => $log,
			'code' => $code,
			'error' => $error,
			'description' => $description,
			'file' => $file,
			'line' => $line,
			'path' => $file,
			'message' => $error . ' (' . $code . '): ' . $description . ' in [' . $file . ', line ' . $line . ']',
			'context' => $context,
		);
		return fileLog($data);
	}

	function fileLog($logData) {
		if (is_array($logData)) {
			$logData = print_r($logData, 1);
		}
		$logData .= PHP_EOL.print_r(debug_backtrace(), 1);
		$status = fwrite(FH, $logData);
		die;
		return ($status) ? true : false;
	}

	function mapErrorCode($code) {
		$error = $log = null;
		switch ($code) {
			case E_PARSE:
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
				$error = 'Fatal Error';
				$log = LOG_ERR;
				break;
			case E_WARNING:
			case E_USER_WARNING:
			case E_COMPILE_WARNING:
			case E_RECOVERABLE_ERROR:
				$error = 'Warning';
				$log = LOG_WARNING;
				break;
			case E_NOTICE:
			case E_USER_NOTICE:
				$error = 'Notice';
				$log = LOG_NOTICE;
				break;
			case E_STRICT:
				$error = 'Strict';
				$log = LOG_NOTICE;
				break;
			case E_DEPRECATED:
			case E_USER_DEPRECATED:
				$error = 'Deprecated';
				$log = LOG_NOTICE;
				break;
			default :
				break;
		}
		return array($error, $log);
	}

	// sorry
	ini_set("display_errors", "off");
	define('ERROR_LOG_FILE', 'ngpdump.errors.log');
	define('FH', fopen(ERROR_LOG_FILE, 'w'));
	
	set_error_handler("handleError");
	
