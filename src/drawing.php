<?php

	function PrintGalSprites()
	{
		global $im, $COLORS, $rom_bytes;
		$im = imagecreatetruecolor(CHARIMG_WIDTH, CHARIMG_HEIGHT);
		$COLORS = [imagecolorallocate($im, 196, 0, 196), imagecolorallocate($im, 255, 255, 255), imagecolorallocate($im, 128, 128, 128), imagecolorallocate($im, 0, 0, 0), imagecolorallocate($im, 128, 0, 128), imagecolorallocate($im, 0, 255, 0), imagecolorallocate($im, 0, 128, 0) ];
		
		imagefill($im, 0, 0, $COLORS[4]);
		
		$j = $i=0;
		
		for ($char_offset = FIRST_CHAR; $char_offset < LAST_CHAR; $char_offset += 4)
		{
			$frames_offset = unpack('V', substr($rom_bytes, AddrRam2Rom($char_offset), 4))[1];
			$pointers_offset = unpack('V', substr($rom_bytes, AddrRam2Rom($char_offset+0x30), 4))[1];

			$animations = [];
			$filled = [];
			$is_fx = false;
			
			if ($char_offset == LAST_CHAR-4)
			{
				$is_fx=true;
				$frames_offset += 4;
			}
			
			for ($frame_offset = $frames_offset; $frame_offset < $pointers_offset; $frame_offset += 4)
			{
				$ptr = unpack('V', substr($rom_bytes, AddrRam2Rom($frame_offset)))[1];
				$peek = unpack('V', substr($rom_bytes, AddrRam2Rom($frame_offset+4)))[1];
				
				if (key_exists($ptr, $filled))
					continue;
				
				$diff = $peek-$ptr;
				if ($diff <= 0) $diff = 515;
				
				$animations[] = GetGalAnimData($ptr, $diff);
				
				$filled[$ptr] = true;
			}
			unset($filled);
			
			$FRAMES_PER_ROW = 10;
			$x = 0;
			$XSTEP = 150;
			$YSTEP = 128;
			$y = 0;
			$displayed_frame = [];
			
			$count = 0;
			
			foreach ($animations as $animation)
			{
				foreach ($animation as $frame)
				{
					$frame_id = $frame['frame'];
					if ($frame_id == 255)
						continue;
					if (key_exists($frame_id, $displayed_frame))
						continue;
					
					$displayed_frame[$frame_id] = true;
					$data_ptr_loc = $pointers_offset+$frame_id*(4*2);
					$data_ptr = unpack('V2', substr($rom_bytes, AddrRam2Rom($data_ptr_loc)));
					$tsa_addr = $data_ptr[1];
					$gfx_addr = $data_ptr[2]+2;

					$gfx_len = unpack('v', substr($rom_bytes, AddrRam2Rom($data_ptr[2])))[1]/8;
					// once *(tsa_addr++) = 1, we are at fx data
					if (!$is_fx)
					{
						$tsa_info = unpack('C2', substr($rom_bytes, AddrRam2Rom($tsa_addr)));
						if ($tsa_info[2] != 1)
							$is_fx = true;
					}
					
					$framedata = GetGalFrameData($tsa_addr, $is_fx);
					$tileinfo = DecodeGalFrame($framedata, $gfx_addr, $is_fx);

					$x = 60 + $i++*$XSTEP;
					$y = 0 + $j*$YSTEP;
					
					if (is_null($tileinfo))
					{
						break;
					}
					DrawTiles($tileinfo, $x, $y);
						
					
					if ($i%$FRAMES_PER_ROW == 0)
					{
						$i=0;
						++$j;
					}
					
				}
				
				//$i=0;
				//++$j;
			}
			//break;
		}
		
		imagepng($im, DIR_OUTPUT.DIRECTORY_SEPARATOR.GAME.'-gfx.png');
		imagedestroy($im);
	}
	
	function PrintAllChars()
	{
		global $im, $COLORS;
		$im = imagecreatetruecolor(CHARIMG_WIDTH, CHARIMG_HEIGHT);
		$COLORS = [imagecolorallocate($im, 196, 0, 196), imagecolorallocate($im, 255, 255, 255), imagecolorallocate($im, 128, 128, 128), imagecolorallocate($im, 0, 0, 0), imagecolorallocate($im, 128, 0, 128), imagecolorallocate($im, 0, 255, 0), imagecolorallocate($im, 0, 128, 0) ];
		
		imagefill($im, 0, 0, $COLORS[4]);
		
		$j = $i=0;
		
		for ($offset = FIRST_CHAR; $offset < LAST_CHAR; $offset += count($data))
		{
			$data = GetCharFrameData($offset);
			$tileinfo = DecodeCharFrame($data);

			$base_x = 60 + $i++*80;
			$base_y = 60 + $j*80;
			DrawTiles($tileinfo, $base_x, $base_y);
			
			if ($i%18 == 0)
			{
				$i=0;
				++$j;
			}
		}
		imagepng($im, DIR_OUTPUT.DIRECTORY_SEPARATOR.GAME.'-gfx.chars.png');
		imagedestroy($im);
	}
	
	function PrintAllFx()
	{
		global $im, $COLORS;
		$im = imagecreatetruecolor(FXIMG_WIDTH, FXIMG_HEIGHT);
		$COLORS = [imagecolorallocate($im, 196, 0, 196), imagecolorallocate($im, 255, 255, 255), imagecolorallocate($im, 128, 128, 128), imagecolorallocate($im, 0, 0, 0), imagecolorallocate($im, 128, 0, 128), imagecolorallocate($im, 0, 255, 0), imagecolorallocate($im, 0, 128, 0) ];
		
		imagefill($im, 0, 0, $COLORS[4]);
		$j = $i=0;

		for ($offset = FIRSTFX; $offset < LASTFX; $offset += count($data))
		{
			$data = GetFxFrameData($offset);
			$tileinfo = DecodeFxFrame($data);
			
			$x = 60 + $i++*210;
			$y = 100 + $j*150;
			DrawTiles($tileinfo, $x, $y);
			
			if ($i%10 == 0)
			{
				$i=0;
				++$j;
			}
		}
		
		imagepng($im, DIR_OUTPUT.DIRECTORY_SEPARATOR.GAME.'-gfx.fx.png');
		imagedestroy($im);
	}
	
	function DrawTiles(array $tileinfo, int $base_x, int $base_y)
	{
		global $im, $COLORS;
		
		imagesetpixel($im, $base_x, $base_y, $COLORS[5]);
		
		for ($i=0; $i < count($tileinfo['tiles']); ++$i)
		{
			$tile = $tileinfo['tiles'][$i];
			$xoffset = $tileinfo['x'][$i];
			$yoffset = $tileinfo['y'][$i];
			$flipped = isset($tileinfo['flip'][$i])? $tileinfo['flip'][$i]: false;
			$mirrored = isset($tileinfo['mirror'][$i])? $tileinfo['mirror'][$i]: false;
			
			if (is_null($tile))
				$pixels = array_fill(0, TILE_WIDTH*TILE_HEIGHT, 6);
			else
				$pixels = DecodeTile($tile, $flipped, $mirrored);
			
			$j=$x=$y=0;
			foreach ($pixels as $pixel)
			{
				imagesetpixel($im, $x+$base_x+$xoffset, $y+$base_y+$yoffset, $COLORS[$pixel]);
				++$j;
				if ($j%8 == 0)
				{
					$y++;
					$x=0;
				}
				else
					$x++;
			}
		}	
	}
