<?php


	function RunTests()
	{
		TestUtils();
		TestFrameDecoders();
	}
	
	function TestUtils()
	{
		$aram = 0x26c6b0; $arom = 0x6c6b0;
		$bram = 0x9b286c; $brom = 0x3b286c;
		
		assert(AddrRam2Rom($aram) == $arom);
		assert(AddrRam2Rom($bram) == $brom);
		
		assert(AddrRom2Ram($arom) == $aram);
		assert(AddrRom2Ram($brom) == $bram);
	}

	function TestFrameDecoders()
	{
		// test fx
		if (GAME == 'svc')
		{
			$frame = ['F0', 'F4', '04', '03', '14', '00', '08', '00', 
				'00', '01', '02', '03', '04', '05', '06', '07', '40', '41', '42', '43',
				'BB', '11', 'BC', '11', 'BD', '11', 'BE', '11', 
				'BF', '11', 'C0', '11', 'C1', '11', 'C2', '11',
			];
			$expected['tiles'] = [0x11BB, 0x11BC, 0x11BD, 0x11BE, 
								 0x11BF, 0x11C0, 0x11C1, 0x11C2,
								 0x11BB, 0x11BC, 0x11BD, 0x11BE
			];
			foreach ($expected['tiles'] as &$num)
				$num = FX_START+($num<<4);
			$expected['flip'] = [false, false, false, false, false, false, false, false, true, true, true, true];
			$expected['mirror'] = [false, false, false, false, false, false, false, false, false, false, false, false];
			$expected['x'] = [-16, -8,  0,  8, -16, -8,  0,  8, -16, -8,  0,  8];
			$expected['y'] = [-12,-12,-12,-12,  -4, -4, -4, -4,   4,  4,  4,  4];
			$result = DecodeFxFrame($frame);
			assert($result == $expected, 'DecodeFXFrame for SVC is not working as expected!');
			unset($frame); unset($expected);
	
			// test chars
			$frame = ['02', '00', 'cf', '09', 'ff', 'd8', 'e6', '09', 'ff', 'e0'];
			$expected['tiles'] = [0x9CF, 0x9E6];
			foreach ($expected['tiles'] as &$num)
				$num = CHARFX_START+($num<<4);
			$expected['x'] = [-1, -1];
			$expected['y'] = [-40, -32];
			$result = DecodeCharFrame($frame);
			assert($result == $expected, 'DecodeCharFrame for SVC did not return as expected!');
			
			unset($num);
			unset($frame);
			unset($expected);
			unset($result);
			$frame = [
				'17', '00', 'cf', '09', 'ff', 'd8', 'ff', 'ff', '04', '00', 'e6', '09',
				'ff', 'e0', 'ff', 'ff', '02', '00', 'ea', '09', 'f7', 'd0', 'd8', '09',
				'f7', 'e0', 'ff', 'ff', '03', '00', 'ec', '09', 'f7', 'e8', 'ff', 'ff',
				'04', '00', 'ef', '09', 'ef', 'd0', 'df', '09', 'ef', 'f0', 'f3', '09',
				'ef', 'f8', 'f4', '09', 'e7', 'd7', 'd4', '09', '0f', 'ef', 'ff', 'ff',
				'03', '00', 'f5', '09', '07', 'dd', 'e5', '09', '07', 'f9',
			];
			$expected['tiles'] = [ 
				0x09cf, 0x09e6, 0x09e7, 0x09e8, 0x09e9, 0x09ea, 0x09eb, 0x09d8,
				0x09ec, 0x09ed, 0x09ee, 0x09ef, 0x09f0, 0x09f1, 0x09f2, 0x09df,
				0x09f3, 0x09f4, 0x09d4, 0x09f5, 0x09f6, 0x09f7, 0x09e5,
			];
			foreach ($expected['tiles'] as &$num)
				$num = CHARFX_START+($num<<4);
			unset($num);
			$expected['x'] = [
				-1, -1, -1, -1, -1, -9, -9, -9, -9, -9, -9, 
				-17, -17, -17, -17, -17, -17, -25, 0x0F, 0x07, 0x07, 0x07, 0x07
			];
			$expected['y'] = [
				-40, -32, -24, -16, -8, -48, -40, -32, -24, -16, -8,
				-48, -40, -32, -24, -16, -8, -41, -17, -35, -27, -19, -7
			];
			$result = DecodeCharFrame($frame);
			assert($result == $expected, 'decodecharframe for long frame from svc did not return as expected!');
		}
		
		if (GAME == 'snkgalsj' || GAME == 'snkgals')
		{
			$frame = [ 3, 
				1, 0x10, 0x18, 0x40, 
				2, 0x10, 0x20, 0x40, 
				3, 0x10, 0x28, 0x40];
			$expected['tiles'] = [0x200000, 0x200010, 0x200020];
			$expected['flip'] = $expected['mirror'] = [false, false, false];
			$expected['x'] = [0x18, 0x20, 0x28];
			$expected['y'] = [0x40, 0x40, 0x40];
			
			$result = DecodeGalFrame($frame, 0x200000, false);
			assert($result == $expected);
			
		}
		
		if (GAME == 'samsho')
		{
			
		}
	}