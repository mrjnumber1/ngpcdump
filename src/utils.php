<?php 

	function AddrRam2Rom(int $addr)
	{
		assert($addr > RAMROM_START);
		
		if ($addr >= RAMHIROM_START)
		{
			$addr -= RAMHIROM_START;
			$addr += HIROM_BEGIN;
		}
		else
			$addr -= RAMROM_START;
		return $addr;
	}
	
	function AddrRom2Ram(int $addr)
	{
		if ($addr >= HIROM_BEGIN)
			$addr += RAMHIROM_START-RAMROM_START;
		else
			$addr += RAMROM_START;
		return $addr;
	}
	
	
	function DetermineROM(array $httpgetvars, array $arguments)
	{
		$rom_filename = null;
		
		if (count($arguments) > 1)
			$rom_filename = $arguments[1];
		else if (!empty($httpgetvars) && array_key_exists('rom', $httpgetvars))
			$rom_filename = $httpgetvars['rom'];
		
		assert(isset($rom_filename), 'No ROM specified in input!');
		
		return $rom_filename;
	}
	

	
	function ReadROM(string $rom_filename)
	{
		assert(is_dir(DIR_ROMS), 'ROM directory not found!');
		
		$full_rom_path = DIR_ROMS.DIRECTORY_SEPARATOR.$rom_filename;
		
		$fp = null;
		assert(file_exists($full_rom_path), 'ROM `'.$rom_filename.'` not found!!');
		assert(filesize($full_rom_path) == ROM_LENGTH, 'ROM must be '.ROM_LENGTH.' bytes');
		assert($fp = fopen($full_rom_path, 'rb'), 'Could not open ROM');
		assert(($rom_bytes = fread($fp, ROM_LENGTH)) !== false, 'Could not read ROM');
		
		fclose($fp);
		
		return $rom_bytes;
	}
	
	function CreateOutputDirIfNotExists(string $dir)
	{
		if (!file_exists(DIR_OUTPUT) && !is_dir(DIR_OUTPUT))
		mkdir(DIR_OUTPUT);
		assert(is_dir(DIR_OUTPUT), 'Output folder not found! Please create "output" directory manually!');
	}