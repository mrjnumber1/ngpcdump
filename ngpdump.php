<?php
	
	assert_options(ASSERT_BAIL, true);
	error_reporting(E_ALL);
	$required_files = ['errors', 'utils', 'const', 'drawing', 'parsing', 'testing'];
	foreach ($required_files as $requirement)
		require 'src'.DIRECTORY_SEPARATOR.$requirement.'.php';

	$rom_filename = DetermineROM($_GET, $argv);
	DefineROMGlobals(pathinfo($rom_filename, PATHINFO_FILENAME), $CONST);
	$rom_bytes = ReadROM($rom_filename);
	
	RunTests();
	
	
	$im = $COLORS = null;
	
	switch (GAME)
	{
	case 'svc':
	case 'kofr1':
	case 'kofr2':
		PrintAllChars();
		unset($im); unset($COLORS);
		$im = $COLORS = 0;
		PrintAllFx();
		break;
	case 'snkgals':
	case 'snkgalsj':
		PrintGalSprites();
		break;
	}

