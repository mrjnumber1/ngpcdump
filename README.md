# ngpcdump
Dump sprites from some of SNK's NGPC fighters.

# Usage
* Place Target ROM in `roms` directory
* Web: navigate to `domain/ngpcdump.php?rom=*yourromname.ext* `
* CLI: `php ngpcdump.php *yourromname*`

# Target ROMs

The ROMs use the same names as designated by MAME. Italicized ROMs aren't finished yet!
Obviously, it's up to you to find them yourself

*  *fatfury.ngp*
*  kofr1.ngp
*  kofr2.ngc
*  *lastblad.ngc*
*  *samsho.ngp*
*  *samsho2.ngc*
*  snkgals.ngc
*  snkgalsj.ngc
*  svc.ngc